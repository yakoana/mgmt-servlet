package org.crypthing.things.appservice.management;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import org.crypthing.things.appservice.management.rest.AgentRest;
import org.crypthing.things.appservice.management.rest.RogueServers;
import org.crypthing.things.appservice.management.rest.ServerRest;
import org.crypthing.things.appservice.management.rest.lame.EventWriter;
import org.crypthing.things.appservice.management.rest.lame.MapSet;
import org.crypthing.things.appservice.management.rest.lame.ServerWriter;

@ApplicationPath("api")
public class RestConfig extends Application {
  public Set<Class<?>> getClasses() {
    Set<Class<?>> classes = new HashSet<>();
    classes.add(MapSet.class);
    classes.add(ServerRest.class);
    classes.add(AgentRest.class);
    classes.add(RogueServers.class);
    classes.add(ServerWriter.class);
    classes.add(EventWriter.class);
    return classes;
  }
}
