package org.crypthing.things.appservice.management.web;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebFilter("/*")
public class CORSFilter implements Filter
{
	@Override public void init(final FilterConfig filterConfig) throws ServletException {}
	@Override
	public void doFilter
	(
		final ServletRequest request,
		final ServletResponse response,
		final FilterChain chain
	)	throws IOException, ServletException
	{
		final HttpServletRequest req = (HttpServletRequest) request;
		final HttpServletResponse resp = (HttpServletResponse) response;
		String reqHeaders = req.getHeader("AccessControlRequestHeaders");
		if(reqHeaders == null) { reqHeaders = ""; }
		resp.addHeader("AccessControlAllowHeaders","Origin, XRequestedWith, ContentType, Accept, " + reqHeaders);
		resp.addHeader("AccessControlAllowOrigin", "*");
		resp.addHeader("AccessControlAllowMethods","GET, OPTIONS, HEAD, PUT, POST, DELETE");
		if (req.getMethod().equals("OPTIONS")) { resp.setStatus(HttpServletResponse.SC_ACCEPTED); return;}
		chain.doFilter(request, response);
   }
	@Override public void destroy() {}
	
}
