package org.crypthing.things.appservice.management.web;

import java.util.Enumeration;
import java.util.Properties;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.crypthing.things.appservice.management.mib.MIB;

public class Initialize extends HttpServlet
{
	private static final long serialVersionUID = 102027804425666283L;
	private MIB mib;
	@Override
	public void init(final ServletConfig config) throws ServletException
	{
		final Properties props = new Properties();
		Enumeration<String> param = config.getInitParameterNames();
		while (param.hasMoreElements())
		{
			final String key = param.nextElement();
			props.setProperty(key, config.getInitParameter(key));
		}
		try
		{
			mib = new MIB();
			mib.init(props);
		}
		catch (final Throwable e) { throw new ServletException(e); }
	}
	@Override
	public void destroy() { if (mib != null) mib.destroy(); }
}
